<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header("Access-Control-Allow-Origin: *");

header("Access-Control-Allow-Methods: DELETE");

header('Access-Control-Allow-Headers: Content-Type, Authorization');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('product/{id}', 'Products\ProductsController@getProductDetails');

Route::post('product', 'Products\ProductsController@getAllProducts');

Route::put('product/{id}', 'Products\ProductsController@updateProduct');

Route::delete('product/{id}', 'Products\ProductsController@removeProduct');