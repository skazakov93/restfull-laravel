<?php

use Illuminate\Database\Seeder;
use App\Modules\Products\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 30; $i++) {
        	$price = rand(100, 2000);

        	Product::create([
        		'name' => $faker->name,
        		'price' => $price
        	]);
        }
    }
}
