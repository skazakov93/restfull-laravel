<?php

namespace App\Modules\Products;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Products\Product;

class ProductsController extends Controller
{
    public function getProductDetails($id) {
    	$product = Product::findOrFail($id);

    	return $product;
    }

    public function getAllProducts() {
    	$products = Product::paginate(5);

    	return $products;
    }

    public function updateProduct($id, UpdateProductRequest $request) {
		$product = Product::findOrFail($id);

		$product->name = $request->input('name');
		$product->price = $request->input('price');

		$product->save();

		// return $product;

		return response()->json($request->all());
    }

    public function removeProduct($id) {
    	$product = Product::findOrFail($id);

    	$product->delete();

    	return $product;
    }
}
